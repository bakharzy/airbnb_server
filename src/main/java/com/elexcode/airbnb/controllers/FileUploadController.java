/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.controllers;

import com.elexcode.airbnb.services.FileService;
import com.mongodb.gridfs.GridFSDBFile;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.gridfs.GridFsCriteria.whereFilename;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileUploadController {
    
    private FileService fileService;
    
    @Autowired
    public FileUploadController(FileService fileService) {
        this.fileService = fileService;
    }
    
    

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    public @ResponseBody
    String provideUploadInfo() throws IOException {
        List<GridFSDBFile> results = fileService.find(query(whereFilename().is("ara")));
        for (GridFSDBFile result : results) {
            System.out.println(result.getFilename());
            System.out.println(result.getContentType());
            result.writeTo("/home/mohi/test/ara");
        }
        return "Done!";
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public @ResponseBody
    String handleFileUpload(@RequestParam("name") String name,
            @RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                fileService.store(file.getInputStream(), name);
                return "You successfully uploaded " + name + "!";
            } catch(Exception e)  {
                return "Failed to upload " + name + ", because of: " + e.getMessage();
            }
        } else {
            return "Failed to upload " + name + " , because the file is empty!";
        }
    }

}
