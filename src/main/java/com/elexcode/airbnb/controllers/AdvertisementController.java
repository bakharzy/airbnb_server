/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.controllers;

import com.elexcode.airbnb.entities.Advertisement;
import com.elexcode.airbnb.services.AdvertisementService;
import com.elexcode.airbnb.services.FileService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class AdvertisementController {

    private AdvertisementService advertisementService;
    private FileService fileService;

    @Autowired
    public AdvertisementController(AdvertisementService advertisementService, FileService fileService) {
        this.advertisementService = advertisementService;
        this.fileService = fileService;
    }

    @RequestMapping(value = "/ads", method = RequestMethod.GET)
    public @ResponseBody
    List<Advertisement> showAllAds() {
        return advertisementService.findAll();
    }

    @RequestMapping(value = "/ad/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Advertisement getOneAd(@PathVariable String id) {
        System.out.println("Getting ad with ID: "+id);
        return advertisementService.findById(id);
    }

    @RequestMapping(value = "/images", method = RequestMethod.POST)
    public byte[] showOneImage(HttpServletRequest req) throws IOException {

        String fileId = IOUtils.toString(req.getInputStream());
        GridFSDBFile file = fileService.findOne(new Query().addCriteria(Criteria.where("_id").is(fileId)));
        InputStream stream = file.getInputStream();
        return IOUtils.toByteArray(stream);
    }

    @RequestMapping(value = "/ad", method = RequestMethod.POST)
    public @ResponseBody
    Advertisement storeAd(@RequestPart("myad") String ad, @RequestPart(value = "imageList", required = false) MultipartFile file) throws IOException {

        Advertisement jsonAd = new ObjectMapper().readValue(ad, Advertisement.class);

        Advertisement newAd = new Advertisement();
        UUID uuid = UUID.randomUUID();
        newAd.setId(uuid.toString());
        String fileId = "";
        DBObject metadata = new BasicDBObject();
        metadata.put("owner", newAd.getId());

        if (file != null && !file.isEmpty()) {
            try {
                GridFSFile gFile = fileService.store(file.getInputStream(), file.getOriginalFilename(), file.getContentType(), metadata);
                fileId = gFile.getId().toString();
                System.out.println("File successfully uploaded with ID: " + fileId);
            } catch (Exception e) {
                System.out.println("File upload Failed, because of " + e.getMessage());
            }
        }
        newAd = jsonAd;
        List<String> imageList = new LinkedList<String>();
        imageList.add(fileId);
        newAd.setImageList(imageList);
        return advertisementService.save(newAd);

    }

    @RequestMapping(value = "/ad/{id}/remove", method = RequestMethod.POST)
    @ResponseBody
    public Advertisement removeAd(@PathVariable String id) {
        System.out.println("Removing ad with ID: " + id);
        Advertisement toBeRemoved = advertisementService.findById(id);
        return advertisementService.remove(toBeRemoved);
    }

    @RequestMapping(value = "*", method = RequestMethod.OPTIONS)
    @ResponseBody
    public String itisOK(HttpServletResponse response) {
        response.setStatus(200);
        return "status:200";
    }

}
