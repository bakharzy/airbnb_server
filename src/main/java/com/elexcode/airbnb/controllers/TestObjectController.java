/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.controllers;

import com.elexcode.airbnb.entities.TestObject;
import com.elexcode.airbnb.services.TestObjectService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestObjectController {
    
    private TestObjectService testObjectService;

    @Autowired
    public TestObjectController(TestObjectService testObjectService) {
        this.testObjectService = testObjectService;
    }
    
    
    
    @RequestMapping("/")
    public String sayHello(){
        return "Hello Mohi!";
    }
    
    @RequestMapping(value = "/test" , method = RequestMethod.GET)
    @ResponseBody
    public List<TestObject> sayTest(){
        System.out.println("I am here");
        return testObjectService.findAll();
    }
    
    @RequestMapping(value = "/test" , method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public TestObject saveTestObject(@RequestBody TestObject obj){
        System.out.println("This is object: " + obj);
        return testObjectService.save(obj);
        
    }
    
    @RequestMapping(value = "/test" , method = RequestMethod.OPTIONS)
    @ResponseBody
    public String itisOK(){
        System.out.println("I am in Options!");
        return "OK!";
    }
    
}
