/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.services;

import java.util.List;


public interface GenericCrudService<T> {
    
    public T save(T object);
    
    public T findById(String id);
    
    public List<T> findAll();
    
    public T remove(T object);
    
}
