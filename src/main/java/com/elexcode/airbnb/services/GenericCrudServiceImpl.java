/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.services;

import java.util.List;
import org.springframework.core.GenericTypeResolver;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author mohi
 */
public class GenericCrudServiceImpl<T> implements GenericCrudService<T>{
    
    MongoRepository<T, String> mongoRepository;
    Class<T> clazz;

    public GenericCrudServiceImpl(MongoRepository mongoRepository) {
        this.mongoRepository = mongoRepository;
        this.clazz = (Class<T>) GenericTypeResolver.
                resolveTypeArgument(getClass(), GenericCrudServiceImpl.class);
    }
    
    

    @Override
    public T save(T object) {
        return mongoRepository.save(object);
    }

    @Override
    public T findById(String id) {
        return mongoRepository.findOne(id);
    }

    @Override
    public List<T> findAll() {
        return mongoRepository.findAll();
    }

    @Override
    public T remove(T object) {
        mongoRepository.delete(object);
        return object;
    }
    
}
