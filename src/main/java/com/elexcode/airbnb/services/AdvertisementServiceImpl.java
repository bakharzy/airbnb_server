/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.services;

import com.elexcode.airbnb.entities.Advertisement;
import com.elexcode.airbnb.repository.AdvertisementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdvertisementServiceImpl extends GenericCrudServiceImpl<Advertisement>
    implements AdvertisementService{
    
    private AdvertisementRepository advertisementRepository;
    
    @Autowired
    public AdvertisementServiceImpl(AdvertisementRepository advertisementRepository) {
        super(advertisementRepository);
        this.advertisementRepository = advertisementRepository;
    }
    
    
    
}
