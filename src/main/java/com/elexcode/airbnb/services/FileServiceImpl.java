/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.services;

import com.elexcode.airbnb.repository.FileRepository;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import java.io.InputStream;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author mohi
 */
@Service
public class FileServiceImpl implements FileService{
    
    private FileRepository fileRepository;
    
    @Autowired
    public FileServiceImpl(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }
    

    @Override
    public GridFSFile store(InputStream content, String filename) {
        return fileRepository.store(content, filename);
    }

    @Override
    public GridFSFile store(InputStream content, String filename, String contentType, DBObject metadata) {
        return fileRepository.store(content, filename, contentType, metadata);
    }
    

    @Override
    public List<GridFSDBFile> find(Query query) {
        return fileRepository.find(query);
    }

    @Override
    public GridFSDBFile findOne(Query query) {
        return fileRepository.findOne(query);
    }
    
    
    
}
