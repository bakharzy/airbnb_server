/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.services;

import com.elexcode.airbnb.entities.TestObject;
import com.elexcode.airbnb.repository.TestObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestObjectServiceImpl extends GenericCrudServiceImpl<TestObject> 
                implements TestObjectService{

    private TestObjectRepository testObjectRepository;
    
    @Autowired
    public TestObjectServiceImpl(TestObjectRepository testObjectRepository) {
        super(testObjectRepository);
        this.testObjectRepository = testObjectRepository;
    }
    
}
