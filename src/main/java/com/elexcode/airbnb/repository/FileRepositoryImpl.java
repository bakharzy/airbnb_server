/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.repository;

import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import java.io.InputStream;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mohi
 */
@Repository
public class FileRepositoryImpl implements FileRepository{
    
    private GridFsTemplate template;
    
    @Autowired
    public FileRepositoryImpl(GridFsTemplate template) {
        this.template = template;
    }
    
    

    @Override
    public GridFSFile store(InputStream content, String filename) {
        return template.store(content, filename);
    }

    
     @Override
    public GridFSFile store(InputStream content, String filename, String contentType, DBObject metadata) {
        return template.store(content, filename, contentType, metadata);
    }
    
    @Override
    public List<GridFSDBFile> find(Query query) {
        return template.find(query);
    }

    @Override
    public GridFSDBFile findOne(Query query) {
        return template.findOne(query);
    }


   

 

    
}
