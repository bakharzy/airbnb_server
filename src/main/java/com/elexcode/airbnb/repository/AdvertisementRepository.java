/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.repository;

import com.elexcode.airbnb.entities.Advertisement;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface AdvertisementRepository extends MongoRepository<Advertisement, String>{
    
}
