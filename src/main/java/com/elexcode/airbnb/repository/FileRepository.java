/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.repository;

import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import java.io.InputStream;
import java.util.List;
import org.springframework.data.mongodb.core.query.Query;


/**
 *
 * @author mohi
 */
public interface FileRepository {
    
    GridFSFile store(InputStream content, String filename);
    
    GridFSFile store(InputStream content, String filename, String contentType, DBObject metadata);
    
//    GridFSDBFile getResource(String locationPattern);
    
    List<GridFSDBFile> find(Query query);
    
    GridFSDBFile findOne(Query query);
    
}
