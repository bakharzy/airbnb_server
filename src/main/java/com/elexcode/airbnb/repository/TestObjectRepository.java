/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.repository;

import com.elexcode.airbnb.entities.TestObject;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author mohi
 */
public interface TestObjectRepository extends MongoRepository<TestObject, String>{
    
}
