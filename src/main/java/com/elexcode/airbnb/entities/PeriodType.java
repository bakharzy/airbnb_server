/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elexcode.airbnb.entities;

/**
 *
 * @author mohi
 */
public enum PeriodType {
    NIGHT, WEEK, MONTH;
}
