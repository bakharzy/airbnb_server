package com.elexcode.airbnb;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;


@SpringBootApplication // same as @Configuration @EnableAutoConfiguration @ComponentScan
@PropertySource("classpath:application.properties")
public class Application extends SpringBootServletInitializer{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
    

    
    
    public static void main(String[] args){
        SpringApplication app = new SpringApplication(Application.class);
        app.run(args);
        
        
    }
}
